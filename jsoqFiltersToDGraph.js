module.exports = function jsoqFiltersToDGraph(collection, filters){
    let filterKeys = Object.keys(filters);
    if (filterKeys.length===1 && filterKeys[0]==="and"){
        return "("+filters.and.map((filter)=>{return jsoqFiltersToDGraph(collection, filter)}).join(" AND ")+")";
    }
    else if (filterKeys.length===1 && filterKeys[0]==="or"){
        return "("+filters.or.map((filter)=>{
            return jsoqFiltersToDGraph(collection, filter)
        }).join(" OR ")+")";
    }
    else{
        let filter = filters;
        if (["=",":","==","==="].includes(filter.comparison)){
            return `eq(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
        else if (filter.comparison===">"){
            return `gt(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
        else if (filter.comparison===">="){
            return `ge(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
        else if (filter.comparison==="<"){
            return `lt(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
        else if (filter.comparison==="<="){
            return `le(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
        else if (filter.comparison==="{"){
            return `uid_in`;
        }
        else if (filter.comparison==="!{"){
            return `uid_in`;
        }
        else if (filter.comparison==="~"){
            return `regex(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
        else if (filter.comparison==="#"){
            return `allofterms(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)}`;
        }
        else if (filter.comparison==="#~"){
            return `anyofterms(${collection}_${filter.attribute}, ${JSON.stringify(filter.value)})`;
        }
    }
}