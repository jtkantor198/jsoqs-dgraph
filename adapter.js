const dgraph = require("dgraph-js");
const grpc = require("grpc");
const implement = require("implement");
let AssociationIndex = require("./associationsToDGraphSchema.js");

class DGraphAdapter{
    /**
     * Creates database connection
     * @param {*} settings 
     */
    constructor(settings){
        this.getAssociationIndex = AssociationIndex.getAssociationIndex;
        this.getAssociationIndexName = AssociationIndex.getAssociationIndexName
        this.jsoqLookupToDGraph = require("./jsoqLookupToDGraph.js");
        implement(settings, {
            host: "string",
            port: "number"
        })
        this.db = new dgraph.DgraphClient(
            new dgraph.DgraphClientStub(`${settings.host}: ${settings.port}`,grpc.credentials.createInsecure())
        );
    }
    /**
     * Ensures collections are implemented as schema
     * @param {*} collection 
     */
    async ensureCollection(collection){
        //implement(collection, database.collection);
        let model = collection.model;
        let collectionSchema = "";
        for (let attributeName of Object.keys(model.attributes)){
            if (attributeName==="uid"||attributeName==="id"||attributeName==="_id"){
                continue;
            }
            let indicies = ""
            if (model.attributes[attributeName].indicies){
                indicies = model.attributes[attributeName].indicies.join(",");
            }
            else{
                indicies = this.getDefaultIndicies(model.attributes[attributeName].type).join(", ");
            }
            collectionSchema+=`${model.name+"_"+attributeName}: ${this.toDGraphTypeName(model.attributes[attributeName].type)} @index(${indicies}) @count .\n`;
        }
        for (let associationName of Object.keys(model.associations)){
            if (model.associations[associationName].inverse!==undefined){
                collectionSchema+= this.getAssociationIndexName(model.name, associationName, model.associations[associationName])+": uid @reverse .\n";
            }
            else{
                collectionSchema+= this.getAssociationIndexName(model.name, associationName, model.associations[associationName])+": uid .\n";
            }
        }
        console.log(collectionSchema)
        let op = new dgraph.Operation();
        op.setSchema(collectionSchema);
        return await this.db.alter(op);
    }
    async find(lookup, database){
        let res = this.transformFieldNames(
            lookup, database, (await this.db.newTxn().query(this.jsoqLookupToDGraph(lookup, database))).getJson().data 
        );
        return res;
    }
    /**
     * Inserts objects into a collection or list, do upsert but throw error if first level uniqueKey matches or uid included
     * @param {*} lookup 
     * @param {*} item 
     */
    async insert(lookup, item, database){
        //In essenence we must insert items into a collection, then associate them with the records found by lookup
        //console.log(lookup, item, database);
        //Inserts over reverse edges aren't working, have to adjust so queries only use non-reverse edges
        //on each reverse edge, we add uid marker and use in subsequent query
        if (lookup.path[lookup.path.length-1].filters && Object.keys(lookup.path[lookup.path.length-1].filters)){
            throw Error("Items belong to a collection. You cannot insert items into a filtered collection.");
        }
        let finalCollection = lookup.path[lookup.path.length-1].collection;
        let allQueries = [];
        let uidCount = 0;
        let transformFieldNames = async (collection, item, internal)=>{
            let newItem;
            if (item.uid===undefined){
                newItem = {uid: `_:${uidCount}`};
                uidCount++;
            }
            else{
                newItem = {uid: item.uid};
            }
            let collectionObject = database.getCollectionByName(collection);
            if (!(collectionObject instanceof Object)){
                throw Error("Collection "+collection+" is not defined.");
            }
            let attributes = Object.keys(collectionObject.model.attributes);
            let associations = Object.keys(collectionObject.model.associations);
            for (let key of Object.keys(item)){
                //console.log(collectionObject.model.attributes, key);
                if (associations.includes(key)){
                    let association = collectionObject.model.associations[key];
                    let fCol = database.getCollectionByName(association.collection);
                    let associateName = this.getAssociationIndex(collection,key,association);
                    if (associateName[0]==="~"){
                        //Need to create new item with oppose association
                        console.log(associateName, "as",association);
                        if (item[key] instanceof Array){
                            let qs = [];
                            for (let itm of item[key]){
                                let nItem = {...itm[key]}
                                nItem[association.inverse] = {uid: newItem.uid};
                                for (let k of Object.keys(nItem)){
                                    if (fCol.model.attributes[k]&&fCol.model.attributes[k].uniqueKey){
                                        console.log(k)
                                        let results = await database.query(`${fCol.model.name}[${k}=${JSON.stringify(nItem[k])}]`);
                                        if (results[0]){
                                            nItem.uid = results[0].uid;
                                        }
                                    }
                                }
                                qs.push(transformFieldNames(association.collection, nItem))
                            }
                            allQueries.push(...(await Promise.all(qs)))
                        }
                        else{
                            let nItem = {...item[key]}
                            nItem[association.inverse] = {uid: newItem.uid};
                            for (let k of Object.keys(nItem)){
                                console.log(k)
                                if (fCol.model.attributes[k]&&fCol.model.attributes[k].uniqueKey){
                                    let results = await database.query(`${fCol.model.name}[${k}=${JSON.stringify(nItem[k])}]`);
                                    if (results[0]){
                                        nItem.uid = results[0].uid;
                                    }
                                }
                            }
                            allQueries.push(await transformFieldNames(association.collection, nItem))
                        }
                    }
                    else{
                        if (item[key] instanceof Array){
                            item[key] = await Promise.all(item[key].map(async(it)=>{return await transformFieldNames(association.collection, it, true)}));
                        }
                        else{
                            item[key] = await transformFieldNames(association.collection, item[key], true);
                        }
                        newItem[associateName] = item[key];
                    }
                }
                else if (attributes.includes(key)&&key!=="uid"){
                    if (collectionObject.model.attributes[key].uniqueKey&&internal){
                        let results = await database.query(`${collectionObject.model.name}[${key}=${JSON.stringify(item[key])}]`);
                        console.log("res", results)
                        if (results[0]){
                            return {uid: results[0].uid}
                        }
                    }
                    newItem[collection+"_"+key] = item[key];
                }
            }
            return newItem;
        }
        //return await this.insertIntoThing(transformFieldNames(finalCollection, item));
        allQueries.unshift(await transformFieldNames(finalCollection, item));
        console.log("arr: "+JSON.stringify(allQueries, null, 4));
        let mu = (new dgraph.Mutation())
        mu.setSetJson(allQueries)
        mu.setCommitNow(true)
        let res = await this.db.newTxn().mutate(mu)
        console.log(res);
        return res;
    }
    /**
     * Updates a set of items, require uniqueKey match or uid
     * @param {*} lookup 
     * @param {*} item 
     */
    update(lookup, item){
        
        mu = (new dgraph.Mutation())
        mu.setSetJson({uid: "0x2", name: "Ted"})
        mu.setCommitNow(true)
        res = client.newTxn().mutate(mu)
    }
    //General mutation
    upsert(){
        
    }
    startTransaction(){

    }
    endTransaction(){

    }
    
    transformFieldNames(lookup, database, results){
        let collection =  database.getCollectionByName(lookup.path[0].collection);
        let obj = {};
        for (let result of results){
            for (let key of Object.keys(result)){
                if (key==="uid"||key==="id"||key==="_id"){
                    continue;
                }
                for (let attr of Object.keys(collection.model.attributes)){
                    if (key===collection.model.name+"_"+attr){
                        result[attr] = result[key];
                        delete result[key];
                    }
                }
                for (let asso of Object.keys(collection.model.associations)){
                    let colName = collection.model.associations[asso].collection;
                    let association = collection.model.associations[asso];
                    if (key===this.getAssociationIndex(collection.model.name,asso,association)){
                        if (association.type.includes("ToOne")){
                            result[asso]=result[key][0];
                        }
                        else{
                            result[asso]=result[key];
                        }
                        delete result[key];
                        if (!obj[colName]){
                            obj[colName]=[]
                        }
                        if (result[asso] instanceof Array){
                            obj[colName].push(...result[asso]);
                        }
                        else{
                            obj[colName].push(result[asso]);
                        }
                        //this.transformFieldNames({path: [{collection: col}]},database, result[asso]);
                    }
                }
            }
        }
        for (let key of Object.keys(obj)){
            console.log(key,obj[key]);
            this.transformFieldNames({path: [{collection: key}]}, database, obj[key]);
        }
        return results;
    }
    
    
    async insertIntoThing(item){
        let mu = (new dgraph.Mutation())
        mu.setSetJson(item);
        mu.setCommitNow(true);
        return await this.db.newTxn().mutate(mu);
    }
    getDefaultIndicies(type){
        if (type==="string"){
            return ["exact"];
        }
        else if (type==="number"){
            return ["float"]
        }
        else if (type==="integer"){
            return ["int"];
        }
        else if (type==="boolean"){
            return ["bool"]
        }
        else if (type==="datetime"){
            return ["hour"];
        }
    }
    toDGraphTypeName(type){
        if (type==="string"){
            return "string";
        }
        else if (type==="number"){
            return "float";
        }
        else if (type==="integer"){
            return "int";
        }
        else if (type==="boolean"){
            return "bool";
        }
        else if (type==="datetime"){
            return "dateTime";
        }
    }
    
}

module.exports = DGraphAdapter;