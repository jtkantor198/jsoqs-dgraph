var assert = require('assert');
var Database = require("../database.js");
var DGraphAdapter = require("../adapter.js");
var adapter, db;/*
describe("test dgraph", function(){
    it ("adpater makes connections", async function(){
        adapter = new DGraphAdapter({
            host: 'localhost',
            port: 9080
        });
        console.log(adapter.db);
    })
    it("creates schemas", async function(){
        try{
            db = await (new Database({
                adapter: adapter,
                collections: [
                    {model: {name: "person", 
                        attributes: {
                            name: {type: "string"},
                            gender: {type: "string"},
                            age: {type: "number"}
                        },
                        associations: {
                            pets: {
                                inverse: "owner",
                                collection: "pet",
                                type: "oneToMany"
                            },
                            bestie: {
                                collection: "person",
                                type: "oneToOne"
                            }
                        }
                    }},
                    {model: {name: "pet", 
                        attributes: {
                            name: {type: "string"},
                            gender: {type: "string"},
                            age: {type: "number"}
                        },
                        associations: {
                            owner: {
                                inverse: "pets",
                                collection: "person",
                                type: "manyToOne"
                            },
                            fans: {
                                collection: "person",
                                type: "manyToMany"
                            }
                        }
                    }}
                ]
            }))
        }
        catch(err){
            console.log(err);
        }
    })
    it("inserts flat item", async function(){
        let res = await db.query("person<="+JSON.stringify({
           name: "Justin",
           gender: "male",
           age: 24 
        }))
        console.log(res);
        res = await db.query('person[name="Justin"]');
        console.log("result", res);
    })
    it("inserts nested item", async function(){
        let res = await db.query("person<="+JSON.stringify({
           name: "Justin",
           gender: "male",
           age: 24,
           pets: [{
               name: "Callie",
               gender: "female",
               age: 1
           }]
        }))
        console.log(res);
        res = await db.query('person[name="Justin"].pets{name, age, owner{name}}');
        res = await db.query('person[name="Justin"].pets{name, age, owner[name="Justin"]{name}}');
        console.log("result", res);
    })
    it("print things", async function(){
        res = await db.query('person<limit: 5>.pets{name, age, owner{name}}');
        //res = await db.query('person[name="Justin"]<limit: 5>.pets<limit: 2>');
        console.log("result", res);
    })
})*/