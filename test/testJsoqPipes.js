var assert = require('assert');
var JsoqPipesToDGraph = require("../jsoqPipesToDGraph.js");
var jsoqFiltersToDGraph = require("../jsoqFiltersToDGraph.js");
var jsoqLookupToDGraph = require("../jsoqLookupToDGraph.js");
var Database = require("../database.js");
var DGraphAdapter = require("../adapter.js");

let JSOQ = require("jsoqs");
prt = (obj)=>console.log(JSON.stringify(obj, null, 4));

var adapter, db;
describe("tests for support functions", function(){
    it ("testing JsoqPipesToDGraph", async function(){
        let pipes = [{
            name: "limit",
            argument: 3
        },{
            name: "sort",
            argument: {name: -1}
        }, {
            name: "count",
            argument: undefined
        }, {
            name: "keys"
        },
        {
            name: "max",
            argument: "age"
        },
        {
            name: "avg",
            argument: "age"
        }]
        console.log(JsoqPipesToDGraph.jsoqFilterPipesToDGraph("person",pipes))
        console.log(JsoqPipesToDGraph.jsoqFieldPipesToDGraph("person",pipes))
        console.log(JsoqPipesToDGraph.jsoqAggregationPipesToDGraph("person",pipes))
    })
    it ("testing JsoqFiltersToDGraph", async function(){

        let filters = {
                or: [{and: [{
                    attribute: "age",
                    comparison: "<",
                    value: 50
                }, {
                    attribute: "age",
                    comparison: ">",
                    value: 2
                }]},{
                    attribute: "name",
                    comparison: "=",
                    value: "Justin"
                }]
        }
        console.log(jsoqFiltersToDGraph("person", filters))
    })
    it ("testing JsoqFiltersToDGraph", async function(){

        let filters = {
                or: [{and: [{
                    attribute: "age",
                    comparison: "<",
                    value: 50
                }, {
                    attribute: "age",
                    comparison: ">",
                    value: 2
                }]},{
                    attribute: "name",
                    comparison: "=",
                    value: "Justin"
                }]
        }
        console.log(jsoqFiltersToDGraph("person", filters))
    })
    it ("testing createLookupObject", async function(){
        prt(Database.createLookupObject(new JSOQ('person[name="Justin"].pets{name, age, owner[name="Justin"]{name}}').getAst()))
        //prt(new JSOQ('person[name="Justin"]<limit: 5>.pets<limit: 2>').getAst())
        prt(Database.createLookupObject(new JSOQ('person[name="Justin"]<limit: 5>.pets<limit: 2>').getAst()));

    })
    it ("adpater makes connections", async function(){
        adapter = new DGraphAdapter({
            host: 'localhost',
            port: 9080
        });
        console.log(adapter.db);
    })
    it("creates schemas", async function(){
        try{
            db = await (new Database({
                adapter: adapter,
                collections: [
                    {model: {name: "person", 
                        attributes: {
                            name: {type: "string"},
                            gender: {type: "string"},
                            age: {type: "number"}
                        },
                        associations: {
                            pets: {
                                inverse: "owner",
                                collection: "pet",
                                type: "oneToMany"
                            },
                            bestie: {
                                collection: "person",
                                type: "oneToOne"
                            }
                        }
                    }},
                    {model: {name: "pet", 
                        attributes: {
                            name: {type: "string"},
                            gender: {type: "string"},
                            age: {type: "number"}
                        },
                        associations: {
                            owner: {
                                inverse: "pets",
                                collection: "person",
                                type: "manyToOne"
                            },
                            fans: {
                                collection: "person",
                                type: "manyToMany"
                            }
                        }
                    }}
                ]
            }))
        }
        catch(err){
            console.log(err);
        }
    })
    it ("testing JsoqlookUpToDGraph", async function(){
        let lookup1 = Database.createLookupObject(new JSOQ('person[name="Justin"].pets{name, age, owner[name="Justin"]{name}}').getAst());
        let lookup2 = Database.createLookupObject(new JSOQ('person[name="Justin"]<limit: 5>.pets<limit: 2>').getAst());
        
        console.log(jsoqLookupToDGraph(lookup1, db));
        console.log(jsoqLookupToDGraph(lookup2, db));
    })
    it ("testing JsoqlookUpToDGraph", async function(){
        let lookup1 = Database.createLookupObject(new JSOQ('person[name="Justin"].pets<sum: "age">').getAst());
        let lookup2 = Database.createLookupObject(new JSOQ('person[name="Justin"].pets<max: {"field": "age","as": "oldestPet"}>').getAst());
        let lookup3 = Database.createLookupObject(new JSOQ('person[name="Justin"].pets<count>').getAst());
        let lookup4 = Database.createLookupObject(new JSOQ('person[name="Justin"]<sum: "age", max: "age">{gender}').getAst());
        
        //let lookup4 = Database.createLookupObject(new JSOQ('person[name="Justin"]<def: {"personAge": "age"}, sum: "plusTen">.pets<math: {"plusTen": "age+personAge"}>').getAst());

        //let lookup5 = Database.createLookupObject(new JSOQ('person[name="Justin"]<sum: "age">').getAst());
        //let lookup6 = Database.createLookupObject(new JSOQ('person[name="Justin"]<max: {"field": "age","as": "oldestPet"}>').getAst());
        //let lookup7 = Database.createLookupObject(new JSOQ('person[name="Justin"]<count>').getAst());
        
        console.log(jsoqLookupToDGraph(lookup1, db));
        console.log(jsoqLookupToDGraph(lookup2, db));
        console.log(jsoqLookupToDGraph(lookup3, db));
        console.log(jsoqLookupToDGraph(lookup4, db));
        /*console.log(jsoqLookupToDGraph(lookup5, db));
        console.log(jsoqLookupToDGraph(lookup6, db));*/

    })

});