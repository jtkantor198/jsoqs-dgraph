
//Test one to one reverse?
let mu = (new dgraph.Mutation())
mu.setSetJson({
    uid: "0x4e21",
    person_bestie: {
        uid: "0x4e22"
    }
})
mu.setCommitNow(true)
await this.db.newTxn().mutate(mu)
//Test oneToMany
let mu = (new dgraph.Mutation())
mu.setSetJson({
    uid: "0x4e21",
    person_pets: [{
        uid: "0x4e22"
    },
    {
        
    }]
})
mu.setCommitNow(true)
await this.db.newTxn().mutate(mu)