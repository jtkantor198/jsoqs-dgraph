let jsoqPipesToDGraph = require("./jsoqPipesToDGraph");
let getAssociationIndex = require("./associationsToDGraphSchema.js").getAssociationIndex;
let jsoqFiltersToDGraph = require("./jsoqFiltersToDGraph.js");
let jsoqAggregationPipesToDGraph = jsoqPipesToDGraph.jsoqAggregationPipesToDGraph;
let jsoqFilterPipesToDGraph = jsoqPipesToDGraph.jsoqFilterPipesToDGraph;
let jsoqFieldPipesToDGraph = jsoqPipesToDGraph.jsoqFieldPipesToDGraph;

module.exports = function jsoqLookupToDGraph(lookup, database){
    /**
    {
        path: [{
            collection: "string",
            filters: {
                or: [filter],
                and: [filter],
                attribute: "string",
                comparison: "string",
                value: "any"
            }
        }],
        pipes: [{
            pipe:,
            argument:
        }]
    }
    */

   let query = "{\n";
    let collection;
    //Initital collection
    /**
     * Handing the primary collection is tricky for many reasons. First we must add @cascade so lower filters act to remove nodes
     * the same way the primary filter does. This cascade should stop before the fields begin, but because of the syntax we cannot
     * stop it from excluding nodes that don't match the filters in fields. This is an unresolved issue.
     * 
     * The intital collection also has a unique syntax regarding filter pipes. Pipes must be added  (func: ..., pipeName: PipeArg, ...)
     * 
     * Lastly the aggregation pipes have to be a entirely seperate block, forcing the primary block to be a var block
     */
    for (let i=0;i<1;i++){
        let col = lookup.path[i];
        console.log(col);
        let wasAggregated = [];
        let collectionName = col.collection;
        primaryAggregationPipes=jsoqPipesToDGraph.jsoqAggregationPipesToDGraph(col.collection, col.pipes, wasAggregated);
        //query+=associationIndex;
        if (primaryAggregationPipes){
            //make following var and add aggregation blocks
            //query += "var(";
            query+="aggregations(){\n";
            query+=primaryAggregationPipes+"\n";
            query+="}\n";
        }
        collection = database.getCollectionByName(col.collection)
        let queryFunc, queryFilterPipes = ""; 
        if (col.filters){
            queryFunc = jsoqFiltersToDGraph(col.collection, col.filters)
        }
        else{
            let attrs = Object.keys(collection.model.attributes);
            queryFunc = `has(${collectionName}_${attrs[0]})`
        }
        if (col.pipes){
            let pipes = jsoqFilterPipesToDGraph(col.collection, col.pipes);
            if (pipes.length){
                queryFilterPipes = ","+pipes.join(",");
            }
        }
        query += `data (func: ${queryFunc}${queryFilterPipes}) @cascade {\n`
        //query+="){\n"
        if (col.pipes){
            let pipes = jsoqFieldPipesToDGraph(col.collection, col.pipes);
            if (pipes.length){
                query+=pipes.join(",")
            }
        }
        
        console.log(collection.model);
        
        for (let attr of Object.keys(collection.model.attributes)){
            if (wasAggregated.includes(collectionName+"_"+attr)){
                query+=collectionName+"_"+attr+" as "+collectionName+"_"+attr+"\n";
            }
            else{
                if (attr === "uid"){
                    query+=attr+"\n";
                }
                else if ((i===lookup.path.length-1)&&!(lookup.fields&&lookup.fields.length)){
                    query+=collectionName+"_"+attr+"\n";
                }
            }
        }
        
    }
    //Rest of collections
    let collectionName;
    for (let i=1;i<lookup.path.length;i++){
        let col = lookup.path[i];
        collectionName = collection.model.associations[col.collection].collection;
        //We need to deal with aggregations here
        let wasAggregated = [];
        let associationIndex = getAssociationIndex(collection.model.name, col.collection, collection.model.associations[col.collection]);
        query+=jsoqAggregationPipesToDGraph(collectionName, col.pipes, wasAggregated, associationIndex)+"\n"
        query+=associationIndex;
        if (col.filters){
            query+="("+jsoqFiltersToDGraph(collectionName, col.filters)+")"
        }
        if (col.pipes){
            let pipes = jsoqFilterPipesToDGraph(collectionName, col.pipes);
            if (pipes.length){
                query+="("+jsoqFilterPipesToDGraph(collectionName, col.pipes)+")"
            }
        }
        query+="{\n"
        if (col.pipes){
            let pipes = jsoqFieldPipesToDGraph(collectionName, col.pipes);
            if (pipes.length){
                query+=jsoqFieldPipesToDGraph(collectionName, col.pipes)+"\n"
            }
        }
        collection = database.getCollectionByName(collectionName)
        
        for (let attr of Object.keys(collection.model.attributes)){
            if (wasAggregated.includes(collectionName+"_"+attr)){
                query+=collectionName+"_"+attr+" as "+collectionName+"_"+attr+"\n";
            }
            else{
                if (attr === "uid"){
                    query+=attr+"\n";
                }
                else if ((i===lookup.path.length-1)&&!(lookup.fields&&lookup.fields.length)){
                    query+=collectionName+"_"+attr+"\n";
                }
            }
        }
        
    }
    // Decending Fields
    if (lookup.fields&&lookup.fields.length){
        let scope = this;
        function thing(collection, fields){
            for (let field of fields){
                if (collection.model.attributes[field.attribute]){
                    query+=collection.model.name+"_"+field.attribute+"\n";
                }
                else if (collection.model.associations[field.attribute]){
                    query+=getAssociationIndex(collection.model.name, field.attribute, collection.model.associations[field.attribute]);
                    if (field.filters){
                        query+=" @filter("+jsoqFiltersToDGraph(collection.model.associations[field.attribute].collection, field.filters)+")"
                    }
                    if (field.pipes){
                        let pipes = jsoqFilterPipesToDGraph(collection.model.associations[field.attribute].collection, field.filters);
                        if (pipes.length){
                            query+=" @filter("+pipes.join(",")+")"
                        }
                    }
                   
                    if (field.pipes){
                        query+=" @filter("+jsoqFieldPipesToDGraph(collection.model.associations[field.attribute].collection, field.filters)+")"
                    }
                    query+="\n{\n"
                    if (field.fields){
                        collection = database.getCollectionByName(collection.model.associations[field.attribute].collection)
                        if (collection===undefined){
                            if (collection.model.associations[field.attribute].collection===undefined){
                                throw Error(`Must specify collection in association ${field.attribute} in collection ${collection.model.name}`)
                            }
                            throw Error("Collection ")
                        }
                        thing(collection, field.fields);
                    }
                    //We must recurse
                    query+="}\n"
                }
            }
        }
        thing(collection, lookup.fields)
        //pipes
    }

   for (let col of lookup.path){
       query+="}\n";
   }
   query+="}";
   console.log(query);
   return query;
}



