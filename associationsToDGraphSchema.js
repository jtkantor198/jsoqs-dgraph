class AssociationIndex{
    constructor(){}
    static getAssociationIndex(collectionName, associationName, association){
        let associateName = AssociationIndex.getAssociationIndexName(collectionName, associationName, association);
        if (associateName.split("_").indexOf(associationName)===2){
            return associateName;
        }
        else{
            //return "~"+associateName;
            if(collectionName<=association.collection){
                return `${associateName}`;
            }
            else{
                return `~${associateName}`;
            }
        }
    }
    static getAssociationIndexName(collectionName, associationName, association){
        if (association.inverse!==undefined){
            if(collectionName<=association.collection){
                return `${collectionName}_${association.collection}_${associationName}_${association.inverse}`;
            }
            else{
                return `${association.collection}_${collectionName}_${association.inverse}_${associationName}`;
            }
        }
        else{
            if(collectionName<=association.collection){
                return `${collectionName}_${association.collection}_${associationName}`;
            }
            else{
                return `${collectionName}_${association.collection}_${associationName}`;
            }
        }
    }
}

module.exports = AssociationIndex;
