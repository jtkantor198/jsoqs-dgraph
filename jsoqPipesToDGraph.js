let implement = require("implement");
class JsoqPipesToDGraph{
    static pipeType(pipe){
        if (["skip","limit","sort","groupBy"].includes(pipe.name)){
            return "filter";
        }
        if (["count", "min","max","sum","avg","math","keys"].includes(pipe.name)){
            return "field";
        }
    }
    static jsoqFilterPipesToDGraph(collectionName,pipes){
        let results = [];
        for (let pipe of pipes){
            if (pipe.name==="skip"){
                if (typeof pipe.argument === "number"){
                    results.push(`offset: ${pipe.argument}`);
                }
                else{
                    throw Error("pipe 'skip' should have an integer argument");
                }
            }
            if (pipe.name==="limit"){
                if (typeof pipe.argument === "number"){
                    results.push(`first: ${pipe.argument}`);
                }
                else{
                    throw Error("pipe 'limit' should have an integer argument");
                }
            }
            if (pipe.name==="sort"){
                implement.addType('sortDirection', (item)=>{
                    if (item===1||item===-1){
                        return true;
                    }
                    else{
                        return false;
                    }
                });
                /*try {
                    implement(pipe.argument, {
                        "[string]": 'sortDirection'
                    })
                }
                catch(err){
                    throw Error("pipe 'sort' should have a field as it's key and 1 or -1 as a value")
                }*/
                for (let field of Object.keys(pipe.argument)){
                    if (pipe.argument[field]===1){
                        results.push(`orderasc: ${collectionName}_${field}`);
                    }
                    if (pipe.argument[field]===-1){
                        results.push(`orderdesc: ${collectionName}_${field}`);
                    }
                }
            }
            if (pipe.name==="groupBy"){
                if (typeof pipe.argument === "string"){
                    results.push(`@groupby(${pipe.argument})`);
                }
                else{
                    throw Error("pipe 'groupBy' should have an string argument");
                }
            }

        }
        return results;
    }
    static jsoqFieldPipesToDGraph(collectionName,pipes){
        let results = [];
        for (let pipe of pipes){
            /*if (pipe.name==="count"){
                results.push(`count: count(uid)`);
            }*/
            if (pipe.name==="keys"){
                results.push(`_predicate_`);
            }
            if (pipe.name==="math"){
                if (pipe.argument instanceof Object){
                    for (let key of Object.keys(pipe.argument)){
                        let equation = pipe.argument[key];
                        results.push(`${key}: math(${equation})`);   
                    }
                }
                else{
                    throw Error("pipe 'max' should have an string argument");
                }
            }
            //Aggregation must happen at +1 level, if at rool it needs a seperate query block
        }
        return results;
    }
    static jsoqAggregationPipesToDGraph(collectionName, pipes, wasAggregated, associationIndex){
        let results = [];
        if (pipes===undefined){
            return "";
        }
        for (let pipe of pipes){
            //Aggregation must happen at +1 level, if at rool it needs a seperate query block
            if (["max","sum","min","avg"].includes(pipe.name)){
                console.log(pipe);
                let stdObj;
                if (typeof pipe.argument === "string"){
                    stdObj = {field: `${pipe.argument}`,as: `${pipe.name}_${pipe.argument}`};
                }
                else if (pipe.argument instanceof Object){
                    stdObj = pipe.argument;
                }
                else{
                    throw Error("pipe 'max' should have an string argument");
                }
                results.push(`${stdObj.as}: ${pipe.name}(val(${collectionName}_${stdObj.field}))`);
                if (wasAggregated!==undefined){
                    wasAggregated.push(`${collectionName}_${stdObj.field}`)
                }
            }
            /*else if (pipe.name==="count"){
                let stdObj;
                console.log(pipe, collectionName);
                if (typeof pipe.argument === "string"){
                    stdObj = {name: pipe.argument, as: `count_${pipe.argument}`};
                }
                else {
                    stdObj = pipe.argument;
                }
                //need relationship name
                //getAssociationIndex(collectionName, stdObj.field)
                results.push(`${stdObj.as}: ${pipe.name}(${collectionName}_${stdObj.name})`);
            }*/
        }
        console.log(results)
        return results.join("\n");
    }
}

module.exports = JsoqPipesToDGraph;